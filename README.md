# Obligatorisk oppgave 1 - C-programmering med prosesser, tr�der og synkronisering

**Sjekk Tasks mappa for l�ste oppgaver**

## Gruppemedlemmer

* Kacper Lewandowski
* Arturs Umbrasko

## Sjekkliste

* Har navnene p� gruppemedlemmene blitt skrevet inn?	[x]
* Har l�ringsassistenter og foreleser blitt lagt til med leserettigheter?	[x]
* Er issue-tracker aktivert?	[x]
* Er pipeline aktivert, og returnerer pipelinen "Successful"?	[x]