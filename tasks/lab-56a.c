#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>     /* printf */
#include <stdlib.h>    /* exit */
#include <unistd.h>    /* fork */
#include <time.h> /* timestamps */

sem_t semaphore[6];

struct threadData {
  int id;         /* thread number */
  int sec;        /* how many seconds to sleep */
  int signal[6];  /* which threads to signal when done */
};

void* initialise(struct threadData *data, int aid, int asec){

  data->id=aid;
  data->sec=asec;

  return NULL;

}

void* thread(void* arg) {
  struct threadData* data = arg;
  //printf("Thread %d kjører ", data->id); fprintf(stdout, "%lu\n", (unsigned long)time(NULL));
//  printf("\n\n\n%d\n\n\n", data->sec);
  sem_wait(&semaphore[data->id]);
  sleep(data->sec);
  printf("Thread %d kjørte i %d sekunder ", data->id, data->sec); fprintf(stdout, "%lu\n", ((unsigned long)time(NULL) %20));
  for(int i=0;i<6;i++) {
    if(data->signal[i]==1) {
      printf("Thread %d vekker thread %d ", data->id, i); fprintf(stdout, "%lu\n", ((unsigned long)time(NULL) %20));
      sem_post(&semaphore[i]);
     }
  }
  pthread_exit(NULL);
}

int main() {
  pthread_t threads[6];
  struct threadData *threaddata[6];

  for(int i=0;i<6;i++) {
    threaddata[i] = (struct threadData*) malloc(sizeof(struct threadData));
    for(int j=0;j<6;j++) { threaddata[i]->signal[j]=0; }
  }

  initialise(threaddata[0], 0, 1);
  threaddata[0]->signal[1]=1;
  threaddata[0]->signal[4]=1;
  sem_init(&semaphore[threaddata[0]->id], 1, 1);
  pthread_create(&threads[0], NULL, thread, (void *) threaddata[0]);

  initialise(threaddata[2], 2, 3);
  threaddata[2]->signal[3]=1;
  sem_init(&semaphore[threaddata[2]->id], 1, 1);
  pthread_create(&threads[2], NULL, thread, (void *) threaddata[2]);

  initialise(threaddata[1], 1, 2);
  threaddata[1]->signal[3]=1;
  sem_init(&semaphore[threaddata[1]->id], 1, 0);
  pthread_create(&threads[1], NULL, thread, (void *) threaddata[1]);

  initialise(threaddata[3], 3, 2);
  sem_init(&semaphore[threaddata[3]->id], 1, 0);
  pthread_create(&threads[3], NULL, thread, (void *) threaddata[3]);

  initialise(threaddata[4], 4, 3);
  threaddata[4]->signal[5]=1;
  sem_init(&semaphore[threaddata[4]->id], 1, 0);
  pthread_create(&threads[4], NULL, thread, (void *) threaddata[4]);

  initialise(threaddata[5], 5, 3);
  sem_init(&semaphore[threaddata[5]->id], 1, 0);
  pthread_create(&threads[5], NULL, thread, (void *) threaddata[5]);

//  threaddata[0]->id=1;             /* thread number 1 */
//  threaddata[0]->sec=1;            /* how long to sleep */
//  threaddata[0]->signal[1]=1;      /* which threads to wake up when done */
//  threaddata[0]->signal[4]=1;

  printf("Starter programmet ");
  fprintf(stdout, "%lu\n", ((unsigned long)time(NULL) %20));

  for(int i=0;i<6;i++) {
    pthread_join(threads[i], NULL);
  }



  return 0;
}
