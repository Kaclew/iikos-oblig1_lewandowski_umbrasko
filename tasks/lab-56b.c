#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SHARED 1  /* process-sharing if !=0, thread-sharing if =0*/
#define BUF_SIZE 20
#define MAX_MOD 100000
#define NUM_ITER 200

#define NUM_THREADS 10

void* Producer(); /* Producer thread */
void* Consumer(); /* Consumer thread */

sem_t empty;            /* empty: How many empty buffer slots */
sem_t full;             /* full: How many full buffer slots */
sem_t b;                /* b: binary, used as a mutex */

int g_data[BUF_SIZE];   /* shared finite buffer  */
int g_idx;              /* index to next available slot in buffer,
                           remember that globals are set to zero
                           according to C standard, so no init needed  */

int main(void) {
	pthread_t pid[NUM_THREADS], cid[NUM_THREADS];

	// Initialise the semaphores
	sem_init(&empty, SHARED, BUF_SIZE);
	sem_init(&full, SHARED, 0);
	sem_init(&b, SHARED, 1);

	// Create the threads
	printf("main started\n");
	for (intptr_t i = 0; i < NUM_THREADS; i++){
		pthread_create(&pid[i], NULL, Producer, (void*)i);
		pthread_create(&cid[i], NULL, Consumer, (void*)i);
	}

	// And wait for them to finish.
	for (int i = 0; i < NUM_THREADS; i++){
		pthread_join(pid[i], NULL);
		pthread_join(cid[i], NULL);
	}
	printf("main done\n");

	return 0;
}


void *Producer(void* number) {
	int i=0, j;

	intptr_t id = (intptr_t)number;

	while(i < NUM_ITER) {
		// pretend to generate an item by a random wait
		usleep(random()%MAX_MOD);

		// Wait for at least one empty slot
		sem_wait(&empty);
		// Wait for exclusive access to the buffer
		sem_wait(&b);

		// Check if there is content there already. If so, print
    // a warning and exit.
		if(g_data[g_idx] == 1) {
			printf("Producer overwrites!, idx er %d\n",g_idx);
			exit(0);
		}

		// Fill buffer with "data" (ie: 1) and increase the index.
		g_data[g_idx]=1;
		g_idx++;

		// Print buffer status.
		j=0; printf("(Producer %ld, idx is %d): ", id, g_idx);
		while(j < g_idx) { j++; printf("="); } printf("\n");

		// Leave region with exlusive access
		sem_post(&b);
		// Increase the counter of full bufferslots.
		sem_post(&full);

		i++;
	}

	return 0;
}


void *Consumer(void* number) {
	int i=0, j;
	intptr_t id = (intptr_t)number;

	while(i < NUM_ITER) {
		// Wait a random amount of time, simulating consuming of an item.
		usleep(random()%MAX_MOD);

		// Wait for at least one slot to be full
		sem_wait(&full);
		// Wait for exclusive access to the buffers
		sem_wait(&b);

		// Check that the buffer actually contains some data
    // at the current slot.
		if(g_data[g_idx-1] == 0) {
			printf("Consumes nothing!, idx er %d\n",g_idx);
			exit(0);
		}

		// Remove the data from the buffer (ie: Set it to 0)
		g_data[g_idx-1]=0;
		g_idx--;

		// Print the current buffer status
		j=0; printf("(Consumer %ld, idx is %d): ", id, g_idx);
		while(j < g_idx) { j++; printf("="); } printf("\n");

		// Leave region with exclusive access
		sem_post(&b);
		// Increase the counter of empty slots.
		sem_post(&empty);

		i++;
	}

	return 0;

}
