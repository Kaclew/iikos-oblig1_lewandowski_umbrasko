#include <stdio.h>  /* printf */
#include <stdlib.h> /* exit */
#include <sys/wait.h> /* waitpid */
#include <unistd.h> /* fork */
#include <time.h> /* timestamps */

void process(int number, int timex) {
  printf("Prosess %d kjører ", number);
  fprintf(stdout, "%lu\n", (unsigned long)time(NULL));
  sleep(timex);
  printf("Prosess %d kjørte i %d sekunder ", number, timex);
  fprintf(stdout, "%lu\n", (unsigned long)time(NULL));
}

int main() {
  int p[6];
  fprintf(stdout, "%lu\n", (unsigned long)time(NULL));

  // START P0
  // Fork en ny proc
  p[0] = fork();
  // Hvis barn:
  if(p[0] == 0) {
    // Kjør proc func
    process(0, 1);
    // Avslutt child
    exit(0);
  }

  // START P2
  // Fork en ny proc
  p[2] = fork();
  // Hvis barn:
  if(p[2] == 0) {
    // Kjør proc func
    process(2, 3);
    // Avslutt child
    exit(0);
  }

  // VENT P0
  waitpid(p[0], NULL, 0);

  // START P1 OG P4

  // Fork en ny proc
  p[1] = fork();
  // Hvis barn:
  if(p[1] == 0) {
    // Kjør proc func
    process(1, 2);
    // Avslutt child
    exit(0);
  }

  // Fork en ny proc
  p[4] = fork();
  // Hvis barn:
  if(p[4] == 0) {
    // Kjør proc func
    process(4, 3);
    // Avslutt child
    exit(0);
  }

  // VENT P1

  waitpid(p[1], NULL, 0);

  // VENT P2

  waitpid(p[2], NULL, 0);

  // START P3

  // Fork en ny proc
  p[3] = fork();
  // Hvis barn:
  if(p[3] == 0) {
    // Kjør proc func
    process(3, 2);
    // Avslutt child
    exit(0);
  }

  // VENT P4

  waitpid(p[4], NULL, 0);

  // START P5

  // Fork en ny proc
  p[5] = fork();
  // Hvis barn:
  if(p[5] == 0) {
    // Kjør proc func
    process(5, 3);
    // Avslutt child
    exit(0);
  }

  // VENT P3

  waitpid(p[3], NULL, 0);

  // VENT P5

    waitpid(p[5], NULL, 0);

  return 0;
}
